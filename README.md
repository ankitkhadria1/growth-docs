# Growth Platform

URL: https://ankitkhadria.gitlab.io/growth-docs

Built with [docsify](https://docsify.js.org)

For local development run:

```
npm i
npm run dev
```
