---
id: script-install
title: Script installation
sidebar_label: Script installation
---

<!--Content-->

In the case of web messenger, knowledgebase, popups, you have to install some scripts on your website's code.
In this section, we will demonstrate all the use-cases related to this topic.

---
## Quick installation
Basic steps to install scripts.

### Web messenger

#### Step 1: Copy messenger code

1. Go to Settings menu => Appstore. 
2. Click on the “Appstore” menu (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/messenger/i1.png" />

3. Then choose your messenger and click on the install code button from the right side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/messenger/i2.png" />

4. Copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/messenger/i3.png" />

5. If your website is single page app then you can choose a single-page app and copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/messenger/i4.png" />

#### Step 2:  Paste the code in your web source 

6. Paste the code in bottom of the body tag (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/messenger/i5.png" />

#### Step 3: Result 

7. Once you have pasted the code, it will look like this in your web right bottom side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/messenger/i6.png" />

### Pop-Ups

#### Step 1: Copy pop-ups code

1. Go to Pop-Ups menu from left sidebar (see the below figure).  
2. Click on the install code button from the right side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/popup/i1.png" />

3. Copy the code (see the below figure)

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/popup/i2.png" />

<aside class="notice">

+ When you want to set an optional width and height of Popup scaling, it allows 2 flow types namely embedded and pop-ups. Insert code section A and B onto the top of body tag in page of your website.

+ If your pop-up flow type is "Embedded", then you can choose the code included in section “A” (see the above figure).

+ If your pop-up flow type is "Popup" then you can choose the code section “B” (see the above figure).

+ ShoutBox, Dropdown, Slide-in Left, Slide-in Right types are, do not necessary to copy install code.

</aside>

#### Step 2: Paste the code in your web source 

4. Paste the code in bottom of the body tag on every page you want Erxes pop-ups to appear (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/popup/i3.png" />

 
#### Step3: Result

5. Once you have pasted the code, it will look like this in your web (see the below figure)

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/popup/i4.png" />

### Knowledgebase

#### Step 1: Copy knowledgebase code

1. Go to Knowledge Base menu from left sidebar (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i1.png" />

2. Then choose the option that named "Manage Knowledgebase" from dropdown menu (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i2.png" />

3. Copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i3.png" />

<aside class="notice">

+ Do not miss to copy the additional source of section “B” and copy it into top of body tag of the source code.

</aside>

#### Step 2: Paste the code in your web source 

4. Paste the code in bottom of body tag on every page you want Erxes knowledge base to appear (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i4.png" />

#### Step3: Result

5. Once you have pasted the code, it will look like this in your web (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i5.png" />

## Google tag manager
Basic steps to install scripts.

###  Install google tag manager 

1. Log in your google tag manager account.
2. Click on the **Admin** from the dropdown menu, then choose the "Install Google Tag Manager" from the right sidebar (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/1.png" />

3. Copy the code as high in the **head** , then paste it onto head tag of every page of your website (see the below figure). 
4. Copy the code as high in the **body** , then paste it onto body tag of every page of your website (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/2.png" />

5. Paste the code of google tag manager onto the every page of your website (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/3.png" />



### Web messenger

#### Step 1: Copy messenger code

1. Go to Settings menu => Appstore. 
2. Click on the “Appstore” menu (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/4.png" />

3. Then choose your messenger and click on the install code button from the right side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/5.png" />

4. Copy the code of Basic Javascript (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/6.png" />


#### Step 2:  Paste the code in google tag manager

5. Log in your google tag manager account.
6. Click the button “Add a new tag” (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/7.png" />

7. Click the Tag Configuration => Custom HTML (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/8.png" />

8. Paste the messenger code to Tag Configuration (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/9.png" />

9. After paste the code, configure to Triggering for All Pages (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/10.png" />


#### Step 3: Result 

7. Once you have pasted the code, it will look like this in your web right bottom side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/33.png" />


### Pop-Ups

#### Step 1: Copy pop-ups code

1. Log in your Erxes account. 
2. Go to Pop-Ups menu from left sidebar (see the below figure).  
3. Click the install code button which you created the Pop Ups (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/12.png" />

4. Copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/13.png" />


#### Step 2: Paste the code in google tag manager 

5. Log in your google tag manager account.
6. Click the button “Add a new tag” (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/14.png" />

7. Click the Tag Configuration => Custom HTML (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/15.png" />

8. Paste the code of Erxes pop-up (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/16.png" />

9. After paste the code, configure to Triggering for All Pages (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/17.png" />

10. When you edit again the script on Google tag manager, you have to click SAVE, then SUBMIT and PUBLISH it. 

11. If you want to set an optional width and height of Popup scaling, it allows 2 flow types namely embedded and pop-ups. Follow the below steps. 

12. Go to Pop-Ups menu from left sidebar (see the below figure).
13. Click on the install code button from the right side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/18.png" />

14. Copy the code section A or section B.

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/19.png" />


<aside class="notice">

+ If your pop-up flow type is "Embedded", then you can choose the code included in section “A” (see the above figure).

+ If your pop-up flow type is "Popup" then you can choose the code section “B” (see the above figure).

+ ShoutBox, Dropdown, Slide-in Left, Slide-in Right types are, do not necessary to copy install code.
</aside>


15. Paste code onto the body tag of web page source (see the below figure).
 
 <img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/20.png" />


#### Step3: Result

16. Once you pasted the code, it will look like this in your web (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/21.png" />

<aside class="notice">

If you have encountered some mistakes, please make sure following steps: 
1. Verify the flow types and the additional source is right.
2. Check the form id on Google tag manager and verify the form id on the web source.
3. When you edit again the script on Google tag manager, make sure to save it and then submit and publish it.
4. Make sure to configure the trigger in Google tag manager correctly.

</aside>

### Knowledgebase

#### Step 1: Copy knowledgebase code

1. Log in to your erxes account
2. Go to Knowledge Base menu from left sidebar (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/22.png" />

3. Then choose the option that named "Manage Knowledgebase" from dropdown menu (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/23.png" />

4. Copy the install code of knowledge base  (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/24.png" />


#### Step 2: Paste the code in google tag manager 

5. Log in to your google tag manager account.
6. Click the button “Add a new tag” (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/25.png" />

7. Click the Tag Configuration=> Custom HTML (see the below figure).  

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/26.png" />

8. Paste the code of Erxes knowledge base code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/27.png" />

9. After paste the code, configure to Triggering for All Pages (see the below figure).  

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/28.png" />


10. When you edit again the script on Google tag manager, you have to click SAVE, then SUBMIT and PUBLISH it. 

11. If you want to set an optional width and height of Knowledgebase scaling, follow the below steps. 

12. Go to Knowledge Base menu from left sidebar (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i1.png" />

13. Then choose the option that named "Manage Knowledgebase" from dropdown menu (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i2.png" />

14. Copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/29.png" />

15. Paste the code onto the top of body tag in web page source (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/30.png" />

#### Step3: Result

16. Once you pasted the code, it will look like this in your web (see the below figure)

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/32.png" />

## Erxes script manager

Basic steps to install Erxes scripts. 

### Web messenger

#### Step 1: Copy Script manager code

1. Log in your erxes account. 
2. Go to Settings menu => Script manager (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/16.png" />

3. Create new script to click NEW SCRIPT (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/17.png" />

4. Insert name and select the messenger that you created (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/18.png" />

5. Click on the install code button from the list (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/19.png" />

6. Copy the code that you created the messenger by clicking COPY TO CLIPBOARD (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/20.png" />

#### Step 2:  Paste the code onto your script manager

7. Paste the code the top of body tag on every page you want Erxes script manager to appear (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/21.png" />

#### Step 3: Result 

8. Once you have pasted the code, it will look like this in your web right bottom side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/22.png" />

### Pop-Ups

#### Step 1: Copy code of Script manager

1. Log in your erxes account. 
2. Go to Settings menu => Script manager. 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/23.png" />

3. Create new script to click NEW SCRIPT.

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/24.png" />

4. Insert name and select the POP UPS that you created. 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/25.png" />

5. Click on the install code button which you created POP UPS from the list (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/26.png" />

6. Copy the code (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/27.png" />

7. If you want to set an optional width and height of Popup scaling, it allows 2 flow types namely embedded and pop-ups. Go to Pop-ups feature, then select your created pop-ups and click on the install code button from the right side (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/28.png" />

8. Copy the code and paste it in top of body tag (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/29.png" />


<aside class="notice">

+ If your pop-up flow type is "Embedded", then you can choose the source included in  section “A”  (see the above figure).

+ If your pop up-flow type is "Popup" then you can choose the source section “B” (see the above figure).

+ ShoutBox, Dropdown, Slide-in Left, Slide-in Right types are, do not necessary to copy install code.

</aside>

#### Step 2: Paste the code onto your script manager

9. Paste the code the bottom of body tag on every page you want Erxes pop-ups to appear (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/30.png" />

 
#### Step3: Result

10. Once you have pasted the code, it will look like this in your web (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/31.png" />

### Knowledgebase

#### Step 1: Copy knowledgebase code of Script manager

1. Log in to your erxes account. 

2. Go to Settings menu => Script manager.

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/32.png" />

3. Create new script to click NEW SCRIPT.

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/33.png" />

4. Insert name and select the KNOWLEDGEBASE TOPIC that you was created before (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/34.png" />

5. Click on the install code button which you created KNOWLEDGEBASE TOPIC from the list (see the below figure). 

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/35.png" />

6. Copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/27.png" />

7. If you want to set an optional width and height of Knowledgebase scaling, follow the below steps. 

8. Go to Knowledge Base menu from left sidebar (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i1.png" />

9. Then choose the option that named "Manage Knowledgebase" from dropdown menu (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/kb/i2.png" />

10. Copy the code (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/google-tag-manager/29.png" />

11. Paste the code onto the top of body tag in web page source (see the below figure). 



#### Step 2: Paste the code onto your script manager

12. Paste the script code the bottom of body tag on every page that you want Erxes script manager to appear (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/39.png" />

#### Step3: Result

13. Once you have pasted the code, it will look like this in your web (see the below figure).

<img src="https://erxes-docs.s3-us-west-2.amazonaws.com/script-installation/script-manager/40.png" />



