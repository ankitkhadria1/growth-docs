* Introduction

  * [Welcome](README.md)

* User Guide

    * [Initial Setup](user/initial-setup.md)
    * [General Settings](user/general-settings.md)
    * [Team inbox](user/team-inbox.md)
    * [Knowledge Base](user/knowledge-base.md)
    * [Popups](user/popups.md)
    * [Script installation](user/script-install.md)
    * [Contacts](user/contacts.md)
    * [Sales pipeline](user/sales.md)
    * [Sales Team Settings](user/sales-pipeline-settings.md)
    * [Engage](user/engage.md)
    * [Insights](user/insights.md)
    * [Email templates](user/email-templates.md)
    * [Third party integrations](user/third-party-integrations.md)
    * [Signing in](user/signing-in.md)
    * [Profile settings](user/profile-settings.md)
    * [Notifications](user/notification.md)
    * [Mobile Apps](user/mobile-apps.md)
    * [Subscription getting started](user/subscription-getting-started.md)
